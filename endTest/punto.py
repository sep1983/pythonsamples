#!/usr/bin/env python
# -*- coding: utf-8 -*-

from math import sqrt

"""Este es el docstring del modulo"""
class Punto:

  """ Este es el docstring de la clase"""
  def __init__(self, x , y ):
    """Este es el docstring del inicializador de clase"""
    self.x = x 
    self.y = y

  def calcular_distancia(self, punto):
    """Este es el docstring del metodo de clase"""
    distancia = sqrt((self.x - punto.x)**2 + (self.y - punto.y)**2)
    return distancia 
