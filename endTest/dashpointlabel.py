#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
===============
Dashpoint Label
===============

"""
from punto import * 
import warnings
import matplotlib.pyplot as plt

warnings.simplefilter("ignore")  # Ignore deprecation of withdash.

fig, ax = plt.subplots()

"""
===============
Captura de puntos
===============

"""
noPuntos = int(input('Ingrese el numero de puntos a utilizar: '))
DATA = tuple([ input('Nuevo punto separado por coma: ') for _ in  range(noPuntos)])

list1 = [list(r) for r in DATA]
last = list1[-1]
p3 = Punto(int(last[0]), int(last[1]))

total = 0
y_pos = 4.5  
for i in range(len(list1)):
    p1 = Punto(int(list1[i][0]), int(list1[i][1]))
    
    if (len(list1) - 1) >= i+1:
        p2 = Punto(int(list1[i+1][0]), int(list1[i+1][1]))
        total += p1.calcular_distancia(p2)
   
    if(list1[i] != last):
        temp = (list1[i], last, p1.calcular_distancia(p3))
        x_pos = 0.2 
        y_pos -= 0.2
        ax.text(x_pos, y_pos, "Distancia al punto Final: " + str(temp))

# dash_style =
#     direction, length, (text)rotation, dashrotation, push
# (The parameters are varied to show their effects, not for visual appeal).
dash_style = (
    (0, 20, -15, 30, 10),
    (1, 30, 0, 15, 10),
    (0, 40, 15, 15, 10),
    (1, 20, 30, 60, 10))


(x, y) = zip(*DATA)
ax.plot(x, y, marker='o')
for i in range(len(DATA)):
    (x, y) = DATA[i]
    (dd, dl, r, dr, dp) = dash_style[i]
    t = ax.text(x, y, str((x, y)), withdash=True,
                dashdirection=dd,
                dashlength=dl,
                rotation=r,
                dashrotation=dr,
                dashpush=dp,
                )
				
x_pos = 0.2 
y_pos = 4.5 
ax.text(x_pos, y_pos, "Total distancia: " + str(total))
				
ax.set_xlim((0, 5))
ax.set_ylim((0, 5))
ax.set(title="NOTE: Puntos y Distancias.")

plt.show()


